| role         | description                                                                                                                                   |
| ------------ | --------------------------------------------------------------------------------------------------------------------------------------------- |
| iptables | настройка правил сервиса iptables (создание, удаление правил) |



Playbook использует hashicorp vault для хранения секретов, в частности для доступа по ssh к хостам. Чтобы использовать свой инстанс hashicorp vault в среде запуска playbook необходимо определить переменные окружения:

`export ANSIBLE_HASHI_VAULT_USERNAME="<login>"
export ANSIBLE_HASHI_VAULT_PASSWORD="<passwd>"
export VAULT_ADDR="http://<host>:8200"`

а также установить модуль для python:
`pip3 install -r requirements.txt`

Добавлен task для бекапа текущего файла iptables, сохраняет по пути:
/etc/sysconfig/iptables_backup
можно переопределить переменной iptables_backup_path

Чтобы не перетереть уже сущестуюшие правила (т.е. избежать flush) укажите переменную prepare_host = false для каждого host в inventory, ну или в group_vars для группы хостов.

Определить какие порты открывать можно в defaults.
Открытие порта 22 захардкожено умышленно, чтобы случайно не обрезать себе доступ, т.к. последним задается правило запрещающее весь остальное трафик:

iptables -A INPUT -j DROP, 
